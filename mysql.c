#include <my_global.h>
#include <mysql.h>
#include <stdio.h>
#include <string.h>

void parseLexem(char **lexem, char **varArray, char **valArray, int index){
	char *token;
	token = strtok(lexem[index], " =");
	varArray[index] = token;
	token = strtok(NULL, " =");
	valArray[index] = token;
}

int main(int argc, char **argv){
	char *str;
	char *beforeSign;
	int isTableExist = 0;

	char *createTableQuery = (char*)malloc(sizeof(char)*512);
	createTableQuery[0] = '\0';
	strcat(createTableQuery, "create table fromTerminal(");

	char *insertQuerry = (char*)malloc(sizeof(char)*512);
	insertQuerry[0] = '\0';
	strcat(insertQuerry, "insert into fromTerminal (");

	char *alterInsertQuerry = (char*)malloc(sizeof(char)*512);

	if (argc > 1){

		MYSQL *con = mysql_init(NULL);

		if (con == NULL) 
		{
			fprintf(stderr, "%s\n", mysql_error(con));
			exit(1);
		}

		if (mysql_real_connect(con, "127.0.0.1", "root", "159357", NULL, 0, NULL, 0) == NULL) 
		{
			fprintf(stderr, "%s\n", mysql_error(con));
			mysql_close(con);
			exit(1);
		}

		if (mysql_query(con, "CREATE DATABASE terminalCommands")) 
		{
			fprintf(stderr, "%s\n", mysql_error(con));
		}

		if (mysql_query(con, "USE terminalCommands")) 
		{
			fprintf(stderr, "%s\n", mysql_error(con));
		}

		char* argvCopy;
		char *pairParse;
		char *firstPairPart;

		int count = 1;

		argvCopy = (char*)malloc(sizeof(char)*strlen(argv[1]));
		strncpy(argvCopy, argv[1], strlen(argv[1]));

		pairParse = strtok(argvCopy, ";");

		while ((pairParse = strtok(NULL, ";")) != NULL){
			count++;
		}
		strncpy(argvCopy, argv[1], strlen(argv[1]));

		char **variable = (char**)malloc(sizeof(char*)*count);
		char **value = (char**)malloc(sizeof(char*)*count);
		char **pairs = (char**)malloc(sizeof(char*)*count);

		pairParse = strtok(argvCopy, ";");
		pairs[0] = (char*)malloc(sizeof(char)*strlen(pairParse));
		strcpy(pairs[0], pairParse);

		int index = 1;

		while ((pairParse = strtok(NULL, ";")) != NULL){
			pairs[index] = (char*)malloc(sizeof(char)*strlen(pairParse));
			strcpy(pairs[index], pairParse);
			index++;
		}
		
		for(int i = 0; i < count; i++){
			parseLexem(pairs,variable,value,i);
			printf("%s %s\n", variable[i],value[i]);
			strcat(createTableQuery, variable[i]);
			strcat(createTableQuery, " int");

			strcat(insertQuerry, variable[i]);
			
			if (i < count-1){
				strcat(createTableQuery, ",");
			}
			else{
				strcat(createTableQuery, ");");
			}


			if (i < count-1){
				strcat(insertQuerry, ",");
			}
			else{
				strcat(insertQuerry, ") values (");
				for(int i = 0; i < count; i++){
					strcat(insertQuerry, value[i]);

					if (i < count-1){
						strcat(insertQuerry, ",");
					}
					else{
						strcat(insertQuerry, ");");
					}
				}
			}
		}
		
		printf("%s\n", createTableQuery);
		printf("%s\n", insertQuerry);

		if (mysql_query(con, createTableQuery)) 
		{
			fprintf(stderr, "%s\n", mysql_error(con));
			isTableExist = 1;
		}

		if (isTableExist == 1){
			for(int i = 0; i < count; i++){
				alterInsertQuerry[0] = '\0';
				strcat(alterInsertQuerry, "alter table fromTerminal add column ");
				strcat(alterInsertQuerry, variable[i]);
				strcat(alterInsertQuerry, " int;");
				printf("%s\n", alterInsertQuerry);
				
				if (mysql_query(con, alterInsertQuerry)) 
				{
					fprintf(stderr, "%s\n", mysql_error(con));
				}
			}
		}

		if (mysql_query(con, insertQuerry)) 
		{
			fprintf(stderr, "%s\n", mysql_error(con));
		}

		mysql_close(con);
		exit(0);
	}
	else{
		printf("The command had no other arguments.\n");
	}  
}
